*** Settings ***
Library         SeleniumLibrary
Resource        ../divya/elements

*** Keywords ***

*** Test Cases ***
Create New Role
    [Documentation]    Contains all infomration to create a new user role
    [Tags]    UPSAFE-987
    Open Browser    https://www.tocitestaging.net/qaautomation    chrome
    Input Text      //*[@id='username']                           charlie.noel
    Input Password    //*[@id="password"]                         Welcome1$
    Click Element    //*[@id="logon"]/div/button[1]/span   
    Go To    https://www.tocitestaging.net/qaautomation/Role  
    Wait Until Element Is Visible    //*[@id='left-nav-new-btn'] 
    Click Element    //*[@id='left-nav-new-btn']
    Input Text    //*[@id='add-role-name']    guest
    Click Element     //*[@id='add-role-type']/option[1]  
    Click Element    //*[@id='save-role-name-btn']
    Reload Page
    Wait Until Element Is Visible    //*[@id='left-nav-new-btn'] 
    Input Text    //*[@id="left-nav-search"]   guest
    Element Should Be Visible    //*[text()='guest']

    


*** Variables ***
