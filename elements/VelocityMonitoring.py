class VelocityMonitoring:
    # Select All Locations
    LocationFilterSelectBtn = "//*[@id='cb_user-lots-grid']"
    # Save Filtered Locations
    LocationFiltersSaveBtn = "//*[@id='save-user-lots-btn']"
    # Pause Auto refresh Button
    PauseAutoRefreshBtn = "//*[text()='Pause Auto Refresh']"
    # Resume Auto Refresh Button
    ResumeAutoRefreshBtn = "//*[text()='Resume Auto Refresh']"
    # Refresh Now Button
    RefreshNowBtn = "//*[@id='content']/div/div[2]/div[1]/button[2]"
    # Sorting Values by Ascending Order in the Velocity Data Table
    VelocityDataSortByAscending = "//*[@id='jqgh_velocity-data-grid_LicenseState']/span/span[1]"
    # Sorting Values by Descending Order in the Velocity Data Table
    VelocityDataSortByDescending = "//*[@id='jqgh_velocity-data-grid_LicenseState']/span/span[2]"
    # Search Box in the Velocity Data Table Column
    VelocityDataSearchBox = "//*[@id='gs_velocity-data-grid_LicenseState']"
    # Ticket Status Default (All)
    VelocityDataTicketStatusDefault = "//*[@id='gs_velocity-data-grid_Status']/option[1]"
    # Ticket Status Unpaid Within Grace
    VelocityDataTicketStatusUnpaidWithinGrace = "//*[@id='gs_velocity-data-grid_Status']/option[2]"
    # Ticket Status Unpaid
    VelocityDataTicketStatusUnpaid = "//*[@id='gs_velocity-data-grid_Status']/option[3]"
    # Ticket Status Paid
    VelocityDataTicketStatusPaid = "//*[@id='gs_velocity-data-grid_Status']/option[4]"
    # Ticket Status Paid After Grace Time
    VelocityDataTicketStatusPaidAfterGraceTime = "//*[@id='gs_velocity-data-grid_Status']/option[5]"
    # Ticket Status Overtime
    VelocityDataTicketStatusOvertime = "//*[@id='gs_velocity-data-grid_Status']/option[6]"
    # Display records per page All
    VelocityDataRecordsPerPageDefault = "//*[@id='jqg2_center']/table/tbody/tr/td[8]/select"
