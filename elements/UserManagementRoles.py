class UserManagementRoles: 
    NewUserRoleBtn = "//*[@id='left-nav-new-btn']"
    NewUserRoleName = "//*[@id='add-role-name']"
    NewUserRoleCloud = "//*[@id='add-role-type']/option[1]"
    NewUserRoleDevice = "//*[@id='add-role-type']/option[2]"
    NewUserRoleSaveBtn = "//*[@id='save-role-name-btn']"
    # Existing Role Page
    DeleteRoleBtn = "//*[@id='remove-role-btn']"
    # Permissions Tab
    PermissionsTab = "//*[@id='role-permission-tab']"
    RoleCategoryPermissionsSaveBtn = "//*[@id='role-permission-save-btn']"
    RoleCategoryPermissionsUndoBtn = "//*[@id='role-permission-undo-btn']"
    RolePermissionsCheckBox = "//*[@id='cb_role-permission-grid']"
    # IsActive Grid Sort by Ascending
    RolePermissionsIsActiveAsc = "//*[@id='jqgh_role-permission-grid_IsSelected']/span/span[1]"
    # IsActive Grid Sort by Descending
    RolePermissionsIsActiveDesc = "//*[@id='jqgh_role-permission-grid_IsSelected']/span/span[2]"
    # IsActive Drop down options
    RolePermissionsIsActiveAll = "//*[@id='gs_role-permission-grid_IsSelected']/option[1]"
    RolePermissionsIsActiveActive = "//*[@id='gs_role-permission-grid_IsSelected']/option[2]"
    RolePermissionsIsActiveInActive = "//*[@id='gs_role-permission-grid_IsSelected']/option[3]"
    # Permissions Name 
    RolePermissionsName = "//*[@id='jqgh_role-permission-grid_PermissionName']"
    RolePermissionsNameAsc = "//*[@id='jqgh_role-permission-grid_PermissionName']/span/span[1]"
    RolePermissionsNameDesc = "//*[@id='jqgh_role-permission-grid_PermissionName']/span/span[2]"
    RolePermissionsNameSearch = "//*[@id='gs_role-permission-grid_PermissionName']"
    RolePermissionsNameResetSearch = "//*[@id='gview_role-permission-grid']/div[2]/div/table/thead/tr[2]/th[5]/div/table/tbody/tr/td[3]/a"
    # Sub Tab
    RolePermissionsSubTab = "//*[@id='jqgh_role-permission-grid_SubTabName']"
    RolePermissionsSubTabAsc = "//*[@id='jqgh_role-permission-grid_SubTabName']/span/span[1]"
    RolePermissionsSubTabDesc = "//*[@id='jqgh_role-permission-grid_SubTabName']/span/span[2]"
    RolePermissionsSubTabSearch = "//*[@id='gs_role-permission-grid_SubTabName']"
    RolePermissionsSubTabResetSearch = "//*[@id='gview_role-permission-grid']/div[2]/div/table/thead/tr[2]/th[6]/div/table/tbody/tr/td[3]/a"
    # Page Title
    RolePermissionsPageTitle = "//*[@id='jqgh_role-permission-grid_PermissionDesc']"
    RolePermissionsPageTitleAsc = "//*[@id='jqgh_role-permission-grid_PermissionDesc']/span/span[1]"
    RolePermissionsPageTitleDesc = "//*[@id='jqgh_role-permission-grid_PermissionDesc']/span/span[2]"
    RolePermissionsPageTitleSearch = "//*[@id='gs_role-permission-grid_PermissionDesc']"
    RolePermissionsPageTitleResetSearch = "//*[@id='gview_role-permission-grid']/div[2]/div/table/thead/tr[2]/th[7]/div/table/tbody/tr/td[3]/a"
    # Tab
    RolePermissionsTab = "//*[@id='role-permission-grid_Module']"
    RolePermissionsTabAsc = "//*[@id='jqgh_role-permission-grid_Module']/span/span[1]"
    RolePermissionTabDesc = "//*[@id='jqgh_role-permission-grid_Module']/span/span[2]"
    RolePermissionTabSearch = "//*[@id='gs_role-permission-grid_Module']"
    RolePermissionsTabResetSearch = "//*[@id='gview_role-permission-grid']/div[2]/div/table/thead/tr[2]/th[8]/div/table/tbody/tr/td[3]/a"
    # Records Per Page
    RolePermissionsPageRecordsAll = "//*[@id='jqg1_center']/table/tbody/tr/td[8]/select/option[4]"
    # Users Tab
    UsersTab = "//*[@id='role-user-tab']"
    # Users Role Grid Checkbox
    UserRoleGridCheckBox = "//*[@id='cb_role-user-grid']"
    # Is Active 
    UserRoleIsActive = "//*[@id='role-user-grid_IsSelected']"
    # IsActive Grid Sort by Ascending
    UserRoleIsActiveAsc = "//*[@id='jqgh_role-user-grid_IsSelected']/span/span[1]"
    # IsActive Grid Sort by Descending
    UserRoleIsActiveDesc = "//*[@id='jqgh_role-user-grid_IsSelected']/span/span[2]"
    # IsActive Drop down options
    UserRoleIsActiveAll = "//*[@id='gs_role-user-grid_IsSelected']/option[1]"
    UserRoleIsActiveActive = "//*[@id='gs_role-user-grid_IsSelected']/option[2]"
    UserRoleIsActiveInActive = "//*[@id='gs_role-user-grid_IsSelected']/option[3]"
    # Login Name
    UserRoleLoginName = "//*[@id='jqgh_role-user-grid_LoginName']"
    UserRoleLoginNameAsc = "//*[@id='jqgh_role-user-grid_LoginName']/span/span[1]"
    UserRoleLoginNameDesc= "//*[@id='jqgh_role-user-grid_LoginName']/span/span[2]"
    UserRoleLoginNameSearch = "//*[@id='gs_role-user-grid_LoginName']"
    UserRoleLoginNameResetSearch = "//*[@id='gview_role-user-grid']/div[2]/div/table/thead/tr[2]/th[5]/div/table/tbody/tr/td[3]/a"
    # First Name
    UserRoleFirstName = "//*[@id='jqgh_role-user-grid_FirstName']"
    UserRoleFirstNameAsc = "//*[@id='jqgh_role-user-grid_FirstName']/span/span[1]"
    UserRoleFirstNameDesc= "//*[@id='jqgh_role-user-grid_FirstName']/span/span[2]"
    UserRoleFirstNameSearch = "//*[@id='gs_role-user-grid_FirstName']"
    UserRoleFirstNameResetSearch = "//*[@id='gview_role-user-grid']/div[2]/div/table/thead/tr[2]/th[6]/div/table/tbody/tr/td[3]/a"
    # Last Name
    UserRoleLastName = "//*[@id='jqgh_role-user-grid_LastName']"
    UserRoleLastNameAsc = "//*[@id='jqgh_role-user-grid_LastName']/span/span[1]"
    UserRoleLastNameDesc= "//*[@id='jqgh_role-user-grid_LastName']/span/span[2]"
    UserRoleLastNameSearch = "//*[@id='gs_role-user-grid_LastName']"
    UserRoleLastNameResetSearch = "//*[@id='gview_role-user-grid']/div[2]/div/table/thead/tr[2]/th[7]/div/table/tbody/tr/td[3]/a"
    # Records Per Page 
    RoleAssignUsersRecordsAll = "//*[@id='jqg2_center']/table/tbody/tr/td[8]/select/option[4]"






